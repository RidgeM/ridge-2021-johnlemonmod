﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour
{
    public Dialogue dialogueIntro;
    public Dialogue dialogueYou;
    public Dialogue dialogueWho;

    public void OnTriggerEnter(Collider other)
    {
        TriggerDialogue();
    }

    public void OnTriggerExit(Collider other)
    {
        EndDialogue();
    }

    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueSystem>().StartDialogue(dialogueIntro, dialogueYou, dialogueWho, this);
    }

    public void EndDialogue()
    {
        FindObjectOfType<DialogueSystem>().EndDialogue();
    }
}
