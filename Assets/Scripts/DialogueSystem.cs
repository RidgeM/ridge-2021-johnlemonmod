﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueSystem : MonoBehaviour
{
    Queue<string> sentences;
    Queue<string> sentences2;
    Queue<string> sentences3;
    public TextMeshProUGUI textBox;
    public TextMeshProUGUI nameText;
    public GameObject dialogueBox;
    public Button button1;
    public Button button2;
    public Button button3;
    public Button button4;
    public Button button5;
    public Button button6;
    private NPC currentNPC;
    public GameEnding gameEnding;
    private Dialogue dialogueTwo;
    private Dialogue dialogueThree;

    // Start is called before the first frame update
    void Start()
    {
        sentences = new Queue<string>();
        sentences2 = new Queue<string>();
        sentences3 = new Queue<string>();
    }

    public void StartDialogue(Dialogue dialogue1, Dialogue dialogue2, Dialogue dialogue3, NPC npc)
    {
        dialogueTwo = dialogue2;
        dialogueThree = dialogue3;
        sentences.Clear();
        sentences2.Clear();
        sentences3.Clear();
        dialogueBox.SetActive(true);

        nameText.text = dialogue1.name;
        currentNPC = npc;

        foreach(string sentence in dialogue1.sentences){
            sentences.Enqueue(sentence);
        }

        foreach(string sentence in dialogue2.sentences){
            sentences2.Enqueue(sentence);
        }

        foreach(string sentence in dialogue3.sentences){
            sentences3.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        button4.gameObject.SetActive(false);
        button5.gameObject.SetActive(false);
        button6.gameObject.SetActive(false);
        if(currentNPC.name == "DetectiveStone"){
            button4.gameObject.SetActive(true);
            button5.gameObject.SetActive(true);
            button6.gameObject.SetActive(true);
            button1.GetComponentInChildren<TextMeshProUGUI>().text = "John Orange";
            button2.GetComponentInChildren<TextMeshProUGUI>().text = "Crime Ghost";
            button3.GetComponentInChildren<TextMeshProUGUI>().text = "Not yet";
        }
        string sentence = sentences.Dequeue();
        textBox.text = sentence;
    }

    public void DisplayNextSentence2()
    {
        sentences3.Clear();
        if(currentNPC.name == "DetectiveStone"){
            gameEnding.m_IsPlayerCaught = true;
        }
        if(sentences2.Count == 0){
            foreach(string repeatSentence in dialogueTwo.sentences){
                sentences2.Enqueue(repeatSentence);
            }
        }
        string sentence = sentences2.Dequeue();
        textBox.text = sentence;
    }

    public void DisplayNextSentence3()
    {
        sentences2.Clear();
        if(currentNPC.name == "DetectiveStone"){
            gameEnding.m_IsPlayerCaught = true;
        }
        if(sentences3.Count == 0){
            foreach(string repeatSentence in dialogueThree.sentences){
                sentences3.Enqueue(repeatSentence);
            }
        }
        string sentence = sentences3.Dequeue();
        textBox.text = sentence;
    }

    public void ExtraButton1()
    {
        gameEnding.m_IsPlayerCaught = true;
    }

    public void ExtraButton2()
    {
        gameEnding.m_IsPlayerAtExit = true;
    }
    public void ExtraButton3()
    {
        gameEnding.m_IsPlayerCaught = true;
    }

    public void EndDialogue()
    {
        button1.GetComponentInChildren<TextMeshProUGUI>().text = "Who are you?";
        button2.GetComponentInChildren<TextMeshProUGUI>().text = "Who did it?";
        button3.GetComponentInChildren<TextMeshProUGUI>().text = "Goodbye.";
        dialogueBox.SetActive(false);
    }

}
